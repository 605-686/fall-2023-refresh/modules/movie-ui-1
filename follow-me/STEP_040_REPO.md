---
title: Create initial screens
template: main-repo.html
---

Let's create a starter user interface!

In this step, we'll create placeholder screens and simple navigation.

!!! note

    We'll be using a trivial custom navigation scheme for this class. It's not robust, though;
    you should look at available navigation frameworks (such as 
    [Navigating with Compose](https://developer.android.com/jetpack/compose/navigation)
    or others. These can be complex, and everyone has their own preferences on which
    are the best. I didn't want to spend a lot of time on navigation in this class, 
    so I'm keeping it to a simple stack-based approach.

To implement our simple navigation, we need to

   * Define state representing our screens
   * Create a private screen stack in the view model
   * Expose the current top-of-stack screen for the UI to consume
   * Choose the screen to display based on the screen state

First, we {{ find("040-create-screen-state", "create screen state") }} using a
Kotlin `sealed interface`. Sealed interfaces limit possible implementing classes or
objects to only those defined in the current module. This is useful in applications
or libraries because they know exhaustively which possible subclasses exist, and, in the
case of a library, no external users can create new subclasses or implementations.

!!! note

    We're creating a file to hold all of these definitions together. In Kotlin, you
    can define multiple public types inside a single file (vs Java, where you
    can only have a single public type per file.)

We're using a `data class` to represent a movie-display screen. Data classes automatically
generate `equals`, `hashCode`, `toString` and some other interesting functions for all
properties defined in their primary constructor. We'll use this to hold onto the movie
that was selected.

Holding onto the movie is **not** a good idea. If it changes in the data store, we won't
be able to automatically load the changes in the UI. We'll fix this when we set up
our database. For now, we pass the movie for convenience, as we're focusing on the user
interface in this module.

We use a Kotlin `object` for the movie list state. Because the movie list will just
display all movies, we don't need to keep any state in the screen instance for it.
Kotlin objects are singletons; you never create instances of them and can access them
globally. We could use a class here (without data) and create instances, but that doesn't
do anything helpful, as all instances would be effectively equal.

In the view model, we {{ find("040-add-stack", "add a screen stack") }} to track screens
that the user has visited. We keep this stack private; the IU doesn't need to know about it.
This stack defines a `set()` function that updates the backing field, *and*
{{ find("040-set-screen", "sets the current screen") }}. The
{{ find("040-current-screen", "current screen") }} is delegated to
a Compose `MutableState` so it can be accessed and observed for changes from the UI.

Note the `private set` on `currentScreen` - this allows us to modify it from inside the
view model, but it cannot be modified from outside.

The last thing we need to do in the view model, is add
{{ find("040-external-stack-support", "external stack support") }} so the UI
can navigate to a new screen or go back to an existing screen. When these
functions modify the stack, its `set()` updates `currentScreen` automatically.

Now the placeholder screens. We define simple
{{ find("040-movie-display", "movie display") }} and
{{ find("040-movie-list", "movie list") }} screens.

Our `MovieDisplayUi` takes a `Movie` as a parameter and displays a single `Text` to
temporarily represent the screen. `MovieListUi` takes a list of movies and a
`onMovieClicked` callback to indicate to the caller that a movie was selected.
For now, we just display "Movie list", and when it's clicked, 
{{ find("040-tell-caller-movie-selected", "tell the caller") }}
that the first movie in the list was selected.

Finally, we define a 
{{ find("040-starter-ui", "starter UI") }}. I like to define a top-level composable
function as a starting point. All it does is collect data from the view model and
pass whatever is needed to the {{ find("040-screen-switch", "current screen") }}. If the
current screen is `null`, that means we've popped all screens off the stack and should
exit the application. This is done by passing an
{{ find("040-exit-app", "event function") }}
that calls `finish()` in the `Activity`, ending the application (because we only
have a single activity in the application.)

!!! note

    Following this approach, the top-level UI function should be the **only** composable
    function to be passed the view model. This ensures that lower-level composable
    functions are more easily testable, as you can pass them just the data they need,
    and not a view model that needs to be set up.

This application can now be run, showing the placeholder movie list screen. When clicked,
the placeholder movie display screen is pushed on the stack and becomes visible. When back is
pressed, we return to the movie list screen. Pressing back again exits the application.

## All code changes
