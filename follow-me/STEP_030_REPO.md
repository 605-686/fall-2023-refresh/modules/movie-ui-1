---
title: Create data
template: main-repo.html
---

Now we create some fake data to display Movies in the app. After we talk about databases,
we'll convert the data into something real (and add Actors and Ratings).

{{ find("030-create-movie", "Create a `Movie`") }} class. For now we just hold a title and
description, but we'll add more when we create our database.

Next, we {{ find("030-create-vm", "create a `MovieViewModel`") }} to hold a list of movies.
View models prepare and provide data for our user interface to consume. Here we hardcode
the data, but we'll change that in the database module.

Finally, we {{ find("030-connect-vm", "connect the view model") }} to the `MainActivity`.
Using the `viewModels` function creates a property delegate that will create an instance
of the specified view model (if it doesn't exist), or fetch an existing one for the
activity. This allows us to keep data across configuration changes, when the activity is
destroyed and recreated.

## All code changes
