---
title: Initial Movie UI
---

We'll start our Movie application with a very simple user interface.

![Movie List](images/movie-list.png){ width=250 }
![Movie List](images/movie-display.png){ width=250 }

All data is hardcoded for this module; we're just creating a very basic user interface.

We'll improve on the user interface in later modules.

Source code for examples is at
[https://gitlab.com/605-686/fall-2023-refresh/modules/movie-ui-1](https://gitlab.com/605-686/fall-2023-refresh/modules/movie-ui-1)