---
title: Update versions
template: main-repo.html
---

Starting with a clean project, we need to update our versions to match those in
[Course hardware and software](../introduction/STEP_010_FULL.md).

!!! note

    If you do not have `gradle/libs.versions.toml` in your project, you didn't choose the
    **Gradle Version Catalog** option when creating your project. I recommend deleting
    the project and starting again, making sure you choose the version catalog option
    under **Build configuration language**.

Be sure to {{ find("020-add-toolchain", "add") }} the `jvmToolchain` to `app/build.gradle.kts`,
and {{ find("020-delete-compat", "delete") }} the `sourceCompatibility`, `targetCompatibility` and `jvmTarget`.

After updating the files, you'll need to re-synchronize them with Android Studio. Android Studio reads the build scripts to determine which modules and which dependencies are used so it can provide code-assist and lint checks in the IDE.

!!! note

    Make sure you update to the versions specified in [Course hardware and software](../introduction/STEP_010_FULL.md)! The versions
    you see here are the last ones I've edited, and may be a few terms old.

When you change a build script, Android Studio will normally display a banner at the top of the file indicating that it needs to be re-synchronized, and you can click "Sync Now" to do so. Otherwise you can click the elephant icon on the toolbar to perform this synchronization.

## All code changes
