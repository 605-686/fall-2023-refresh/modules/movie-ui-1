---
title: Flesh out the screens
template: main-repo.html
---

Now we flesh out the screens, making a very simple (but inefficient) list, and
a simple display.

First, we create a couple of helper components for consistency.

{{ find("050-display", "`Display`") }} and {{ find("050-label", "`Label`") }}
are just `Text`s with some parameters set for styling and padding. Using this
approach of creating wrapper functions gives us "extended" components.

!!! note

    Note how `Display` has 
    {{ find("050-two-padding", "*two* padding modifiers") }}.
    These are applied in the order they're defined. In this case,
    we apply an 8dp padding all around, and then an extra 16dp padding only at
    the start of the text. This causes the text to appear indented.

    If we did something like

    ```kotlin
        Modifier
            .padding(8.dp)
            .border(2.dp, Color.Blue, RoundedCornerShape(4.dp))
            .padding(8.dp)
    ```

    we'd see 8dp padding, a blue border inside it, and 8dp more padding inside
    the border. This is a much simpler system than the margin/padding combinations
    that existed with the old "views" UI approach.

We surround each of the screens with a
{{ find("050-scaffold", "`Scaffold`") }}, which is a
["slot api"](https://developer.android.com/jetpack/compose/layouts/basics#slot-based-layouts)
composable function. This is an implementation of the **Template Method Pattern**,
an algorithm with replaceable steps. The `Scaffold` defines an algorithm for managing
several "slots" on the screen, areas like "stuff at the top", "stuff at the bottom",
"stuff in between", etc. `Scaffold` defines the layout algorithm for measuring and placing
elements in the specified position; you define what goes in those positions.

In this example, we define {{ find("050-top", "`topBar`") }}, passing a lambda for
what elements should appear in that section of the `Scaffold`. When the scaffold
is performing its layout work, it calls that lambda to obtain the components, measures
them, and places them. (Note that some slotted apis may conditionally execute
lambdas such as these; for example, if you define a navigation drawer, it might not
appear unless the user has swiped in from the side.)

Our `topBar` declares that a `TopAppBar` should appear. This is a common tool bar
element, which can contain a title, navigation icon, and action icons. We'll do
much more with it in later modules. Here we're just setting a title.

Our `MovieDisplay` sets up a 
{{ find("050-movie-display-column", "`Column`") }}
as the main content of the `Scaffold`. Note the `paddingValues` that are passed to
the content lambda. These define the padding you must use to avoid overlapping with
other slots in the `Scaffold`. 

Our `Column` is scrollable and stacks `Label`s and `Display`s to show the details
of a movie.

`MovieListUi` is a bit more complex. It uses a `Scaffold` in the same way as `MovieDisplay`,
but the contents of its column are
{{ find("050-movie-list", "dynamic") }}. We iterate through the list of movies
using `forEach`, and create a nice little `Card` for each movie, showing an icon and title.

We make the icon and title line up nicely by adding an
{{ find("050-alignment", "alignment specification") }}.

By defining `onClick` {{ find("050-onclick", "at the `Card` level") }}, the
user can click anywhere inside the card to select the movie. That click passes the
movie for that card to `onMovieClicked`, informing the caller that a movie has been
selected.

Be sure to define any literal strings that the user will see in 
{{ find("050-strings", "`app/src/main/res/values/strings.xml`") }}.

## All code changes
