package com.androidbyexample.movieui1

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.androidbyexample.movieui1.screens.MovieList
import com.androidbyexample.movieui1.screens.Screen

// ##START 030-create-vm
class MovieViewModel: ViewModel() {
    // ##START 040-add-stack
    private var screenStack = listOf<Screen>(MovieList)
        set(value) {
            field = value
            // ##START 040-set-screen
            currentScreen = value.lastOrNull()
            // ##END
        }
    // ##END

    // ##START 040-current-screen
    var currentScreen by mutableStateOf<Screen?>(MovieList)
        private set
    // ##END

    // ##START 040-external-stack-support
    fun pushScreen(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun popScreen() {
        screenStack = screenStack.dropLast(1)
    }
    // ##END

    val movies: List<Movie> = listOf(
        Movie("The Transporter", "Jason Statham kicks a guy in the face"),
        Movie("Transporter 2", "Jason Statham kicks a bunch of guys in the face"),
        Movie("Hobbs and Shaw", "Cars, Explosions and Stuff"),
        Movie("Jumanji - Welcome to the Jungle", "The Rock smolders"),
    )
}
// ##END
