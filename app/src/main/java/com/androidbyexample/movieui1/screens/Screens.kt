package com.androidbyexample.movieui1.screens

import com.androidbyexample.movieui1.Movie

// ##START 040-create-screen-state
sealed interface Screen

object MovieList: Screen
data class MovieDisplay(val movie: Movie): Screen
// ##END
