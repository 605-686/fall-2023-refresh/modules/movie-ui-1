package com.androidbyexample.movieui1.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import com.androidbyexample.movieui1.MovieViewModel

// ##START 040-starter-ui
@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    // ##START 040-back-handler
    BackHandler {
        viewModel.popScreen()
    }
    // ##END

    // ##START 040-screen-switch
    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            MovieDisplayUi(screen.movie)
        }
        MovieList -> {
            MovieListUi(viewModel.movies) { movie ->
                viewModel.pushScreen(MovieDisplay(movie))
            }
        }
    }
    // ##END
}
// ##END