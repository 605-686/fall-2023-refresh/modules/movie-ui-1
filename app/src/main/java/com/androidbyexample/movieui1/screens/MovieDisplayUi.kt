package com.androidbyexample.movieui1.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.androidbyexample.movieui1.Movie
import com.androidbyexample.movieui1.R
import com.androidbyexample.movieui1.components.Display
import com.androidbyexample.movieui1.components.Label

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieDisplayUi(
    movie: Movie,
) {
    // ##START 050-scaffold
    Scaffold(
        // ##START 050-top
        topBar = {
            TopAppBar(
                title = {
                    Text(text = movie.title)
                }
            )
        }
        // ##END
    ) { paddingValues ->
        // ##START 050-movie-display-column
        Column(
            modifier = Modifier
                .padding(paddingValues)
                .verticalScroll(rememberScrollState())
        ) {
            Label(textId = R.string.title)
            Display(text = movie.title)
            Label(textId = R.string.description)
            Display(text = movie.description)
        }
        // ##END
    }
    // ##END
}
