package com.androidbyexample.movieui1

// ##START 030-create-movie
data class Movie(
    val title: String,
    val description: String,
)
// ##END
