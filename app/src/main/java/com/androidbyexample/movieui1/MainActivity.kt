package com.androidbyexample.movieui1

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.androidbyexample.movieui1.screens.Ui
import com.androidbyexample.movieui1.ui.theme.MovieUi1Theme

class MainActivity : ComponentActivity() {
    // ##START 030-connect-vm
    private val viewModel by viewModels<MovieViewModel>()
    // ##END

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MovieUi1Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    // ##START 040-exit-app
                    Ui(viewModel) {
                        finish()
                    }
                    // ##END
                }
            }
        }
    }
}
